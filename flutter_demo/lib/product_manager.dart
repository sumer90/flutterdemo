import 'package:flutter/material.dart';

import './Products.dart';
import './secondscreen.dart';

class ProductManager extends StatefulWidget {
  final String startingProduct;
  ProductManager({this.startingProduct='Sweets Tester'});
  @override
  State<StatefulWidget> createState() {
    return _ProductManagerState();
  }
}

class _ProductManagerState extends State<ProductManager> {
  List<String> _products = [];

  @override
  void initState() {
    _products.add(widget.startingProduct);
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.all(10.0),
          child: Column(children: [
            RaisedButton(
              child: Text('Add Product'),
              onPressed: () {
                setState(() {
                  _products.add('Advance food tester');
                });
              },
            ), // Raised button
            RaisedButton(
              child: Text('Go To Screen 2'), onPressed: () {
                setState(() {
                  Navigator.push(
                    context,
                    new MaterialPageRoute(builder: (ctxt) => new SecondScreen()),
                  );
                });
            },
            )//Raised button
          ]), //Column body
        ),
        Products(_products)// Adding Product widget directly from here
      ],
    ); //Container
  }
}
