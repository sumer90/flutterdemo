import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class SecondScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SecondScreenState();
  }
}

class _SecondScreenState extends State<SecondScreen> {
  Future<Post> post;
  int _counter = 0;
  final StreamController<int> _streamController = StreamController<int>();

  @override
  void dispose(){
    _streamController.close();
    super.dispose();
  }



  @override
  Widget build(BuildContext ctxt) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Screen 2'),
          actions: <Widget>[
            new IconButton(
                icon: new Icon(Icons.close),
                onPressed: () {
                  setState(() {
                    Navigator.pop(ctxt); // Pop from stack
                  });
                }),
          ],
        ),
        //Used floating action button to mimic the stream concept as setState() is not used instead data is populated using stream concept
        floatingActionButton: FloatingActionButton(child: const Icon(Icons.add),
            onPressed: (){
              _streamController.sink.add(++_counter);
            }),// Floating action button
        body: new Column(children: <Widget>[
          Container(
            margin: EdgeInsets.all(12.0),
            child: FutureBuilder<Post>(
              future: post,
              builder: (BuildContext context,AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return Text("API RES: "+snapshot.data.title);
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }

                // By default, show a loading spinner
                return new Text('Welcome to Screen 2');
              },
            ),
          ),
          Container(
            margin: EdgeInsets.all(8.0),
            child: RaisedButton(
              child: Text('Call API'),
              onPressed: () {
                setState(() {
                  post = fetchPost();
                  new CircularProgressIndicator();
                });
              },
            ),//Raisedbutton
          )//Container
          ,Center(
            child: StreamBuilder<int>(
                stream: _streamController.stream,
                initialData: _counter,
                builder: (BuildContext context, AsyncSnapshot<int> snapshot){
                  return Text('You hit me: ${snapshot.data} times');
                }
            ),
          )
        ]));
  }
}

Future<Post> fetchPost() async {
  final response =
      await http.get('https://jsonplaceholder.typicode.com/posts/1');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    return Post.fromJson(json.decode(response.body));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

class Post {
  final int userId;
  final int id;
  final String title;
  final String body;

  Post({this.userId, this.id, this.title, this.body});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
      body: json['body'],
    );
  }
}
